﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoProjectStructure.BFF.Helpers
{
    public class JwtTokenHelper
    {
        public IConfiguration Configuration { get; }

        public JwtTokenHelper(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string GenerateJwtToken()
        {
            SymmetricSecurityKey SIGNING_KEY = new
                SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["SecretAPIKey"]));
            var credentials = new SigningCredentials(SIGNING_KEY, SecurityAlgorithms.HmacSha256);
            var header = new JwtHeader(credentials);
            DateTime expire = DateTime.UtcNow.AddMinutes(1);
            int ts = (int)(expire - new DateTime(1970, 1, 1)).TotalSeconds;
            var payload = new JwtPayload
            {
                { "exp", ts },
                { "iss", Configuration["BFFURL"] },
                { "aud", Configuration["ServiceURL"] }
            };
            var secToken = new JwtSecurityToken(header, payload);
            var handler = new JwtSecurityTokenHandler();
            var tokenString = handler.WriteToken(secToken);
            return tokenString;
        }
    }
}
