﻿using DemoProjectStructure.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoProjectStructure.Service.IRepositories
{
    public interface IOrderRepository
    {
        public IList<Order> GetAll();
        public Order GetById(int id);
        public IList<Order> GetAllOrdersOfACustomer(int customerID);
    }
}
