﻿using DemoProjectStructure.Service.IRepositories;
using DemoProjectStructure.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoProjectStructure.Service.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly DatabaseContext _context;

        public CustomerRepository(DatabaseContext context)
        {
            this._context = context;
        }

        public IList<Customer> GetAll()
        {
            return this._context.Customers.ToList();
        }

        public Customer GetById(int id)
        {
            return this._context.Customers.Find(id);
        }
    }
}
