﻿using DemoProjectStructure.Service.IRepositories;
using DemoProjectStructure.Service.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoProjectStructure.Service.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly DatabaseContext _context;

        public OrderRepository(DatabaseContext context)
        {
            this._context = context;
        }

        public IList<Order> GetAll()
        {
            return (from item in this._context.Orders.Include(item => item.Customer)
                   select item).ToList();
        }

        public IList<Order> GetAllOrdersOfACustomer(int customerID)
        {
            return (from item in this._context.Orders.Include(item => item.Customer)
                    where item.CustomerId == customerID
                    select item).ToList();
        }

        public Order GetById(int id)
        {
            return (from item in this._context.Orders.Include(item => item.Customer)
                    where item.Id == id
                    select item).FirstOrDefault();
        }
    }
}
