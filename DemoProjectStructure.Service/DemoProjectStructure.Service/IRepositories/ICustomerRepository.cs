﻿using DemoProjectStructure.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoProjectStructure.Service.IRepositories
{
    public interface ICustomerRepository
    {
        public IList<Customer> GetAll();
        public Customer GetById(int id);
    }
}
