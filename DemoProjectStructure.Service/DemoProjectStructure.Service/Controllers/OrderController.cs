﻿using DemoProjectStructure.Service.IRepositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoProjectStructure.Service.Controllers
{
    [Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderRepository _orderReposiroty;

        public OrderController(IOrderRepository orderReposiroty)
        {
            this._orderReposiroty = orderReposiroty;
        }

        [HttpGet]
        public IActionResult GetAllOrders()
        {
            return Ok(this._orderReposiroty.GetAll());
        }

        [HttpGet("{id}")]
        public IActionResult GetOrderById(int id)
        {
            return Ok(this._orderReposiroty.GetById(id));
        }

        [HttpGet]
        public IActionResult GetAllOrdersOfACustomer(int customerID)
        {
            return Ok(this._orderReposiroty.GetAllOrdersOfACustomer(customerID));
        }
    }
}
