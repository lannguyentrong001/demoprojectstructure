﻿using DemoProjectStructure.BFF.Helpers;
using DemoProjectStructure.BFF.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DemoProjectStructure.BFF.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class WebBFFController : ControllerBase
    {
        private readonly HttpClient client = new HttpClient(new RequestRetryHandler(new HttpClientHandler()));
        public IConfiguration Configuration { get; }

        public WebBFFController(IConfiguration configuration)
        {
            Configuration = configuration;
            // Add JWT Token
            client.DefaultRequestHeaders.Authorization =
                    new AuthenticationHeaderValue("Bearer", new JwtTokenHelper(this.Configuration).GenerateJwtToken());
            // Set "Timeout" Attribute
            client.Timeout = new TimeSpan(0, 0, 5);
        }

        [HttpGet]
        public IActionResult GetJWTToken()
        {
            return Ok(new JwtTokenHelper(this.Configuration).GenerateJwtToken());
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCustomers()
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(Configuration["ServiceURL"]);
                stringBuilder.Append("/api/Customer/GetAllCustomers");
                string requestURL = stringBuilder.ToString();
                HttpResponseMessage response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                List<Customer> customers = JsonConvert.DeserializeObject<List<Customer>>(response.Content.ReadAsStringAsync().Result);
                return Ok(customers);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomerById(int id)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(Configuration["ServiceURL"]);
                stringBuilder.Append("/api/Customer/GetCustomerById/{0}");
                string requestURL = String.Format(
                    stringBuilder.ToString(),
                    id);
                HttpResponseMessage response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                Customer customer = JsonConvert.DeserializeObject<Customer>(response.Content.ReadAsStringAsync().Result);
                return Ok(customer);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return StatusCode(500);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllOrders()
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(Configuration["ServiceURL"]);
                stringBuilder.Append("/api/Order/GetAllOrders");
                string requestURL = stringBuilder.ToString();
                HttpResponseMessage response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                List<Order> orders = JsonConvert.DeserializeObject<List<Order>>(response.Content.ReadAsStringAsync().Result);
                return Ok(orders);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return StatusCode(500);
            }
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOrderByID(int id)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(Configuration["ServiceURL"]);
                stringBuilder.Append("/api/Order/GetOrderById/{0}");
                string requestURL = String.Format(
                    stringBuilder.ToString(),
                    id);
                HttpResponseMessage response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                Order order = JsonConvert.DeserializeObject<Order>(response.Content.ReadAsStringAsync().Result);
                return Ok(order);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return StatusCode(500);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllOrdersOfACustomer(int customerID)
        {
            try
            {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.Append(Configuration["ServiceURL"]);
                stringBuilder.Append("/api/Order/GetAllOrdersOfACustomer?customerID={0}");
                string requestURL = String.Format(
                    stringBuilder.ToString(),
                    customerID);
                HttpResponseMessage response = await client.GetAsync(requestURL);
                response.EnsureSuccessStatusCode();
                List<Order> orders = JsonConvert.DeserializeObject<List<Order>>(response.Content.ReadAsStringAsync().Result);
                return Ok(orders);
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught!");
                Console.WriteLine("Message :{0} ", e.Message);
                return StatusCode(500);
            }
        }
    }
}
